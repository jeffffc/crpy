from setuptools import setup, find_packages

packages = find_packages(exclude=['tests*'])

setup(
    name='crpy',
    version='1.0.1',
    packages=packages,
    install_requires=[
        'requests',
        'future'
      ],
    url='',
    license='',
    author='jeffffc',
    author_email='',
    description=''
)
