.. CRpy documentation master file, created by
   sphinx-quickstart on Sun Apr 29 15:42:43 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CRpy's documentation!
================================

.. toctree::
   crpy
   :maxdepth: 2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
