crpy.utils package
==================

Submodules
----------

crpy.utils.helpers module
-------------------------

.. automodule:: crpy.utils.helpers
    :members:
    :undoc-members:
    :show-inheritance:

crpy.utils.league\_statistics module
------------------------------------

.. automodule:: crpy.utils.league_statistics
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: crpy.utils
    :members:
    :undoc-members:
    :show-inheritance:
