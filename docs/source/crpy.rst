crpy package
============

Subpackages
-----------

.. toctree::

    crpy.utils

Submodules
----------

crpy.achievement module
-----------------------

.. automodule:: crpy.achievement
    :members:
    :undoc-members:
    :show-inheritance:

crpy.arena module
-----------------

.. automodule:: crpy.arena
    :members:
    :undoc-members:
    :show-inheritance:

crpy.base module
----------------

.. automodule:: crpy.base
    :members:
    :undoc-members:
    :show-inheritance:

crpy.battle module
------------------

.. automodule:: crpy.battle
    :members:
    :undoc-members:
    :show-inheritance:

crpy.card module
----------------

.. automodule:: crpy.card
    :members:
    :undoc-members:
    :show-inheritance:

crpy.chest module
-----------------

.. automodule:: crpy.chest
    :members:
    :undoc-members:
    :show-inheritance:

crpy.clan module
----------------

.. automodule:: crpy.clan
    :members:
    :undoc-members:
    :show-inheritance:

crpy.client module
------------------

.. automodule:: crpy.client
    :members:
    :undoc-members:
    :show-inheritance:

crpy.constants module
---------------------

.. automodule:: crpy.constants
    :members:
    :undoc-members:
    :show-inheritance:

crpy.deck module
----------------

.. automodule:: crpy.deck
    :members:
    :undoc-members:
    :show-inheritance:

crpy.error module
-----------------

.. automodule:: crpy.error
    :members:
    :undoc-members:
    :show-inheritance:

crpy.games module
-----------------

.. automodule:: crpy.games
    :members:
    :undoc-members:
    :show-inheritance:

crpy.player module
------------------

.. automodule:: crpy.player
    :members:
    :undoc-members:
    :show-inheritance:

crpy.stats module
-----------------

.. automodule:: crpy.stats
    :members:
    :undoc-members:
    :show-inheritance:

crpy.tournament module
----------------------

.. automodule:: crpy.tournament
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: crpy
    :members:
    :undoc-members:
    :show-inheritance:
