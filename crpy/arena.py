"""This module contains an object that represents an Arena."""

from crpy import CRObject


class Arena(CRObject):
    """This object represents an Arena.

    Attributes:
        name (:obj:`str`): Name of the arena.
        arena (:obj:`str`): Name of the arena.
        arena_id (:obj:`int`): The ID of the arena.
        trophy_limit (:obj:`int`): The trophy limit for this arena.

        """

    def __init__(self,
                 name,
                 arena,
                 arena_id=None,
                 trophy_limit=None,
                 **kwargs):
        # Required
        self.name = name
        self.arena = arena
        self.arena_id = int(arena_id)
        # Optionals
        self.trophy_limit = int(trophy_limit)

        self._id_attrs = (self.arena_id,)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Arena, cls).de_json(data)

        return cls(**data)
