"""A library that provides a Python interface to the RoyaleAPI.com API"""

from .base import CRObject

from .arena import Arena
from .achievement import Achievement
from .battle import Battle
from .chest import Chest
from .clan import Clan
from .card import Card
from .stats import Stats
from .games import Games
from .tournament import Tournament
from .deck import Deck

from .player import Player

from .client import CRClient
from .constants import API_BASE_URL
from .error import InvalidToken, InvalidTag
