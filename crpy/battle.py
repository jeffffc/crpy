"""This module contains an object that represents a Player Battle."""

from crpy import CRObject


class Battle(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 battle_type,
                 utc_time,
                 challenge_type=None,
                 mode=None,
                 win_count_before=None,
                 deck_type=None,
                 team_size=None,
                 winner=None,
                 team_crowns=None,
                 opponent_crowns=None,
                 team=None,
                 opponent=None,
                 arena=None,
                 **kwargs):
        # Required
        self.battle_type = battle_type
        self.utc_time = utc_time
        self.challenge_type = challenge_type
        self.mode = mode
        self.win_count_before = win_count_before
        self.deck_type = deck_type
        self.team_size = team_size
        self.winner = winner
        self.team_crowns = team_crowns
        self.opponent_crowns = opponent_crowns
        self.team = team
        self.opponent = opponent
        self.arena = arena

        self._id_attrs = (self.battle_type, self.utc_time, self.team, self.opponent, self.arena)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Battle, cls).de_json(data)
        if data.get('type'):
            data['battle_type'] = data['type']
        if data.get('time'):
            data['utc_time'] = data['time']
        return cls(**data)

    @classmethod
    def de_list(cls, data):
        if not data:
            return []

        battles = list()
        for battle in data:
            battles.append(cls.de_json(battle))
        return battles
