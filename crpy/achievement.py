"""This module contains an object that represents an Achievment."""

from crpy import CRObject


class Achievement(CRObject):
    """This object represents an Achievement.

    Attributes:
        name (:obj:`str`): Name of the achievement.
        stars (:obj:`int`): Number of stars.
        value (:obj:`int`): Value.
        target (:obj:`int`): Target.
        info (:obj:`int`): Info of this achievement.

        """

    def __init__(self,
                 name=None,
                 stars=None,
                 value=None,
                 target=None,
                 info=None,
                 **kwargs):
        self.name = name
        self.stars = stars
        self.value = value
        self.target = target
        self.info = info

        self._id_attrs = (self.name, self.info)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Achievement, cls).de_json(data)

        return cls(**data)

    @classmethod
    def de_list(cls, data):
        if not data:
            return []

        achievements = list()
        for achievement in data:
            achievements.append(cls.de_json(achievement))
        return achievements
