"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject


class LeagueStatistics(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 current_season,
                 previous_season,
                 best_season=None,
                 **kwargs):
        # Required
        self.current_season = current_season
        self.previous_season = previous_season
        self.best_season = best_season
        # Optionals

        self._id_attrs = (self.current_season, self.previous_season, self.best_season)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(LeagueStatistics, cls).de_json(data)
        data['current_season'] = LeagueStatistic.de_json(data.get('current_season'))
        data['previous_season'] = LeagueStatistic.de_json(data.get('previous_season'))
        data['best_season'] = LeagueStatistic.de_json(data.get('best_season'))

        return cls(**data)


class LeagueStatistic(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 id=None,
                 rank=None,
                 trophies=None,
                 best_trophies=None,
                 **kwargs):
        # Required
        self.id = id
        self.rank = int(rank)
        self.trophies = int(trophies)
        self.best_trophies = int(best_trophies)
        # Optionals

        self._id_attrs = (self.id, self.rank, self.trophies, self.best_trophies)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(LeagueStatistic, cls).de_json(data)

        return cls(**data)
