"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject


class Clan(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 tag,
                 name,
                 description=None,
                 clan_type=None,
                 score=None,
                 member_count=None,
                 required_score=None,
                 donations=None,
                 clan_chest=None,
                 badge=None,
                 location=None,
                 members=None,
                 **kwargs):
        # Required
        self.tag = tag
        self.name = name
        self.description = description
        # Optionals
        self.clan_type = clan_type
        self.score = score
        self.member_count = member_count
        self.required_score = required_score
        self.donations = donations
        self.clan_chest = clan_chest
        self.badge = badge
        self.location = location
        self.members = members

        self._id_attrs = (self.tag,)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Clan, cls).de_json(data)

        return cls(**data)
