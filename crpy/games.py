"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject, Battle


class Games(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 total,
                 tournament_games,
                 wins=None,
                 wins_percent=None,
                 losses=None,
                 losses_percent=None,
                 draws=None,
                 draws_percent=None,
                 last_battle=None,
                 **kwargs):
        # Required
        self.total = total
        self.tournament_games = tournament_games
        self.wins = wins
        # Optionals
        self.wins_percent = wins_percent
        self.losses = losses
        self.losses_percent = losses_percent
        self.draws = draws
        self.draws_percent = draws_percent
        self.last_battle = last_battle

        self._id_attrs = (self.total, self.tournament_games, self.wins, self.wins_percent,
                          self.losses, self.losses_percent, self.draws, self.draws_percent,
                          self.last_battle)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Games, cls).de_json(data)
        data['last_battle'] = Battle.de_json(data.get('last_battle'))

        return cls(**data)
