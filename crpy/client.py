import requests
import os
from future.utils import raise_with_traceback
from crpy.constants import API_BASE_URL
from crpy import Card, Player
from crpy.error import CRError, InvalidToken, InvalidTag

try:
    import ujson as json
except ImportError:
    import json


class CRClient(object):
    def __init__(self, token=None, custom_headers=None):
        self.token = self._validate_token(os.environ.get('CR_TOKEN', token))

        if not token:
            raise_with_traceback(ValueError("No API Token provided."))

        self.session = requests.Session()
        self.headers = dict()
        self.headers['auth'] = token
        if custom_headers:
            self.headers.update(custom_headers)

    @staticmethod
    def _validate_token(token):
        """A very basic validation on token."""
        if any(x.isspace() for x in token):
            raise InvalidToken()

        return token

    @staticmethod
    def _sanitize_tag(tag):
        if any(x.isspace() for x in tag):
            raise InvalidTag()

        return str(tag).upper().replace('0', 'O').replace('1', 'L').replace('I', 'L')

    def _request(self, endpoint):
        url = "{}{}".format(API_BASE_URL, endpoint)
        data = self.session.request('GET', url, headers=self.headers).content
        return self._parse(data)

    @staticmethod
    def _parse(json_data):
        """Try and parse the JSON returned from Telegram.
        Returns:
            dict: A JSON parsed as Python dict with results - on error this dict will be empty.
        """

        try:
            decoded_s = json_data.decode('utf-8')
            data = json.loads(decoded_s)
        except UnicodeDecodeError:
            raise CRError('Server response could not be decoded using UTF-8')
        except ValueError:
            raise CRError('Invalid server response')

        if data.get('error'):  # pragma: no cover
            status = data.get('status')
            message = data.get('message')
            return CRError("{}: {}".format(status, message))
        return data

    def get_stats(self):
        return self._request('auth/stats')

    def get_player(self, tag):
        tag = self._sanitize_tag(tag)
        result = self._request('player/{}'.format(tag))
        return Player.de_json(result)
