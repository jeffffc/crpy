"""This module contains an object that represents a Clash Royale Player."""

from crpy import (CRObject, Battle, Clan, Arena, Stats,
                  Achievement, Games, Chest, Card)


class Player(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 tag,
                 name,
                 trophies,
                 rank,
                 arena=None,
                 clan=None,
                 stats=None,
                 games=None,
                 chest_cycle=None,
                 league_statistics=None,
                 deck_link=None,
                 current_deck=None,
                 cards=None,
                 achievements=None,
                 battles=None,
                 **kwargs):
        # Required
        self.tag = tag
        self.name = name
        self.trophies = int(trophies)
        # Optionals
        self.rank = int(rank)
        self.arena = arena
        self.clan = clan
        self.stats = stats
        self.games = games
        self.chest_cycle = chest_cycle
        self.league_statistics = league_statistics
        self.deck_link = deck_link
        self.current_deck = current_deck or list()
        self.cards = cards or list()
        self.achievements = achievements
        self.battles = battles or list()

        self._id_attrs = (self.tag,)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Player, cls).de_json(data)
        data['arena'] = Arena.de_json(data.get('arena'))
        data['clan'] = Clan.de_json(data.get('clan'))
        data['stats'] = Stats.de_json(data.get('stats'))
        data['chest_cycle'] = Chest.de_json(data.get('chest_cycle'))
        data['games'] = Games.de_json(data.get('games'))
        data['current_deck'] = Card.de_list(data.get('current_deck'))
        data['cards'] = Card.de_list(data.get('cards'))
        data['battles'] = Battle.de_list(data.get('battles'))
        data['achievements'] = Achievement.de_list(data.get('achievements'))
        return cls(**data)

    def to_dict(self):
        data = super(Player, self).to_dict()

        # Required
        # Optionals
        if self.battles:
            data['current_deck'] = [d.to_dict() for d in self.current_deck]
        if self.battles:
            data['cards'] = [c.to_dict() for c in self.cards]
        if self.battles:
            data['battles'] = [b.to_dict() for b in self.battles]
        if self.achievements:
            data['achievements'] = [a.to_dict() for a in self.achievements]
        return data
