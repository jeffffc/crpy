"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject


class Stats(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 tournament_cards_won,
                 max_trophies,
                 three_crown_wins=None,
                 cards_found=None,
                 favourite_card=None,
                 total_donations=None,
                 challenge_max_wins=None,
                 challenge_cards_won=None,
                 level=None,
                 **kwargs):
        # Required
        self.tournament_cards_won = int(tournament_cards_won)
        self.max_trophies = int(max_trophies)
        self.three_crown_wins = int(three_crown_wins)
        # Optionals
        self.cards_found = int(cards_found)
        self.favourite_card = favourite_card
        self.total_donations = int(total_donations)
        self.challenge_max_wins = int(challenge_max_wins)
        self.challenge_cards_won = int(challenge_cards_won)
        self.level = int(level)

        self._id_attrs = (self.level, self.challenge_cards_won, self.challenge_max_wins, self.favourite_card,
                          self.cards_found, self.total_donations, self.three_crown_wins, self.max_trophies,
                          self.tournament_cards_won)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Stats, cls).de_json(data)

        return cls(**data)
