"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject


class Tournament(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 tag,
                 tournament_type,
                 status,
                 name,
                 description=None,
                 capacity=None,
                 max_capacity=None,
                 preparation_duration=None,
                 duration=None,
                 ended_time=None,
                 start_time=None,
                 create_time=None,
                 creator=None,
                 members=None,
                 **kwargs):
        # Required
        self.tag = tag
        self.tournament_type = tournament_type
        self.status = status,
        # Optionals
        self.name = name
        self.description = description
        self.capacity = capacity
        self.max_capacity = max_capacity
        self.preparation_duration = preparation_duration
        self.duration = duration
        self.ended_time = ended_time
        self.start_time = start_time
        self.create_time = create_time
        self.creator = creator
        self.members = members

        self._id_attrs = (self.tag,)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Tournament, cls).de_json(data)

        return cls(**data)
