"""This module contains an object that represents a Player Chest."""

from crpy import CRObject


class Chest(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 upcoming,
                 super_magical,
                 magical,
                 legendary,
                 epic,
                 giant,
                 **kwargs):
        # Required
        self.upcoming = upcoming
        self.super_magical = super_magical
        self.magical = magical
        self.legendary = legendary
        self.epic = epic
        self.giant = giant

        self._id_attrs = (self.upcoming, self.super_magical, self.magical, self.legendary, self.epic, self.giant)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Chest, cls).de_json(data)

        return cls(**data)
