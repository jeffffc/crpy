"""This module contains an object that represents a Clash Royale Player."""

from crpy import CRObject


class Deck(CRObject):
    """This object represents a Clash Royale Player.
    Attributes:
        later
    """

    def __init__(self,
                 arena,
                 description,
                 elixir,
                 icon,
                 id=None,
                 key=None,
                 max_level=None,
                 name=None,
                 rarity=None,
                 card_type=None,
                 **kwargs):
        # Required
        self.arena = arena
        self.description = description
        self.elixir = elixir
        # Optionals
        self.icon = icon
        self.id = int(id)
        self.key = key
        self.max_level = max_level
        self.name = name
        self.rarity = rarity
        self.card_type = card_type

        self._id_attrs = (self.key,)

    @classmethod
    def de_json(cls, data):
        if not data:
            return None

        data = super(Deck, cls).de_json(data)

        return cls(**data)

    @classmethod
    def de_list(cls, data):
        if not data:
            return []

        decks = list()
        for deck in data:
            decks.append(cls.de_json(deck))
            return decks
