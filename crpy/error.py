class CRError(Exception):
    def __init__(self, message):
        super(CRError, self).__init__()
        self.message = message

    def __str__(self):
        return '%s' % self.message


class InvalidToken(CRError):
    def __init__(self):
        super(InvalidToken, self).__init__('Invalid token')


class InvalidTag(CRError):
    def __init__(self):
        super(InvalidTag, self).__init__('Invalid tag')
